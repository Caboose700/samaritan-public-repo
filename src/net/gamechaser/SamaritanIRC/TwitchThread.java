package net.gamechaser.SamaritanIRC;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import org.json.simple.parser.ParseException;

import net.gamechaser.SamaritanIRC.apiTools.Twitch;

public class TwitchThread implements Runnable{
	
	BufferedWriter writer;
	String subject;
	Map<Integer, String> names;
	Map<Integer, String> urls;
	ArrayList<Integer> knownStreaming;
	
	public void run() {
		while(true) {
			try { Thread.sleep(30000); } catch (InterruptedException e) { e.printStackTrace(); }
			try { this.knownStreaming = Twitch.check(writer, subject, names, urls, knownStreaming); } catch (Exception e) { e.printStackTrace(); }
		}
	}
	
	public TwitchThread(BufferedWriter writer, String subject, Map<Integer, String> names, Map<Integer, String> urls) throws ParseException, IOException, InterruptedException {

		this.writer = writer;
		this.subject = subject;
		this.names = names;
		this.urls = urls;
		
		// Build up list of people we know we are streaming initially.
		this.knownStreaming = Twitch.check(writer, subject, names, urls);
	}
}
