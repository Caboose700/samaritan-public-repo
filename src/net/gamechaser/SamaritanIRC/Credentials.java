package net.gamechaser.SamaritanIRC;

import java.util.ArrayList;

public class Credentials {

    private static String OS = System.getProperty("os.name").toLowerCase();
    static int Debug = 0;

    // FTP Data
    public static ArrayList<String> GetFTPData() {
        ArrayList<String> results = new ArrayList<String>();
        results.add(""); // FTP Server
        results.add(""); // FTP Username
        results.add(""); // FTP Password
        return results;
    }

    // Image Data
    public static ArrayList<String> GetImageData() {
        ArrayList<String> results = new ArrayList<String>();

        if(OS.indexOf("win") >= 0) {
            results.add(""); // Background Image Location
            results.add(""); // Generated Image Save Location (Where you want it to be saved)
        } else {
            results.add(""); // Background Image Location
            results.add(""); // Generated Image Save Location (Where you want it to be saved)
        }
        return results;
    }

    // IRC Data
    public static ArrayList<String> GetIRCData() {
        ArrayList<String> results = new ArrayList<String>();

        results.add("");  // IRC Server
        if(Debug == 1) {
            results.add(""); // IRC Nick Name
            results.add(""); // IRC Password
            results.add(""); // IRC Realname
            results.add(""); // IRC Channel
        } else {
            results.add(""); // IRC Nick Name
            results.add(""); // IRC Password
            results.add(""); // IRC Realname
            results.add(""); // IRC Channel
        }
        return results;
    }

    // Twitch API Key
    public static String getTwitchURL() {
        return "https://api.twitch.tv/kraken/streams?channel=pixeltailgames,macdguy,foohy,gamedev11,lifeless2011,zakblystone";
    }

    // Google API Key
    public static String getYouTubeAPI() {
        return "";
    }

    // OpenWeatherMap
    public static String getOpenWeatherMapAPI() {
        return "";
    }

    // Merriam-Webster
    public static String getDictionaryAPI() {
        return "";
    }
}
