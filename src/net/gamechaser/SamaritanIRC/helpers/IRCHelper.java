package net.gamechaser.SamaritanIRC.helpers;

import java.io.BufferedWriter;
import java.io.IOException;

public class IRCHelper {
	
	public static void write(BufferedWriter writer, String subject, String message) throws IOException {
		writer.write("PRIVMSG " + subject + " :" + message + "\r\n");
		writer.flush();
	}
	
	public static void write(BufferedWriter writer, String subject, String[] message) throws IOException, InterruptedException {
		for(int i = 0; i<message.length; i++) {
			try { writer.write("PRIVMSG " + subject + " :" + message[i] + "\r\n"); } catch (IOException e) { e.printStackTrace(); }
		}
		writer.flush();
	}
	
	public static void notice(BufferedWriter writer, String subject, String message) throws IOException {
		writer.write("NOTICE " + subject + " :" + message + "\r\n");
		writer.flush();
	}
	
	public static void notice(BufferedWriter writer, String subject, String[] message) throws IOException {
		for(int i = 0; i<message.length; i++) {
			try { writer.write("NOTICE " + subject + " :" + message[i] + "\r\n"); } catch (IOException e) { e.printStackTrace(); }
		}
		writer.flush();
	}
}
