package net.gamechaser.SamaritanIRC.helpers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import net.gamechaser.SamaritanIRC.Credentials;

public class LogHelper {
	
	private static ArrayList<String> IRCStats = Credentials.GetIRCData();
	private static final String mainIRCChannel = IRCStats.get(4);
	
	public static boolean isBlocked(String name) {
		String[] names = {"Global", "NickServ", "ChanServ", "py-ctcp", "peer", "irc.broke-it.com", "irc.cyberdynesystems.net", "irc.rizon.io", 
				"irc.rizon.sexy", "irc.sxci.net", "irc.ku.cx", "irc.x2x.cc", "irc.blackened.org", "irc.rizon.no", "irc.rizon.so",
				"irc.uworld.se", "irc.infoqb.se", "irc.vmnode.pw", "irc.losslessone.com", "TheMachine", "Samaritan", "Samaritan2", "ctcp", "irc.rizon.io"};
		if(Arrays.asList(names).contains(name) || (name == null)) {
			return true;
		}
		return false;
	}
	
	public static void add(String log, String irchandle, String message, String IRCChannel) throws IOException {
		if(!IRCChannel.replaceAll("\\s","").equals(mainIRCChannel)) { return; };
		DateFormat logFormat = new SimpleDateFormat("HH:mm:ss");
		
		Writer output = new BufferedWriter(new FileWriter(log, true));
		output.append("[" + logFormat.format(new Date(System.currentTimeMillis())) +  "] <" + irchandle + ">: " + message + "\r\n");
		output.close();
	}
	
	public static void add(String log, String irchandle, String[] message, String IRCChannel) throws IOException {
		if(!IRCChannel.replaceAll("\\s","").equals(mainIRCChannel)) { return; };
		DateFormat logFormat = new SimpleDateFormat("HH:mm:ss");
		Writer output = new BufferedWriter(new FileWriter(log, true));
		
		for(int i = 0; i < message.length; i++) {
			output.append("[" + logFormat.format(new Date(System.currentTimeMillis())) +  "] <" + irchandle + ">: " + message[i] + "\r\n");
		}
		output.close();
	}
}