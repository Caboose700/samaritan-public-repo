package net.gamechaser.SamaritanIRC;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import org.apache.commons.net.ftp.FTP;

import net.gamechaser.SamaritanIRC.apiTools.Dictionary;
import net.gamechaser.SamaritanIRC.apiTools.GoogleImageSearch;
import net.gamechaser.SamaritanIRC.apiTools.GoogleSearch;
import net.gamechaser.SamaritanIRC.apiTools.Trello;
import net.gamechaser.SamaritanIRC.apiTools.Twitch;
import net.gamechaser.SamaritanIRC.apiTools.Weather;
import net.gamechaser.SamaritanIRC.apiTools.YouTubeSearch;
import net.gamechaser.SamaritanIRC.helpers.FTPHelper;
import net.gamechaser.SamaritanIRC.helpers.IRCHelper;
import net.gamechaser.SamaritanIRC.helpers.LogHelper;

public class Samaritan {
		
	public static void main(String[] args) throws Exception {
		boolean running = true;
		do {
			System.out.println(new Timestamp(System.currentTimeMillis()) + ": Samaritan Online.");
			// Static Strings
			String[] info = {
			"Samaritan Version 6.5.2",
			"System Administrator: Caboose700",
			"Primary    Mandate: Generate Graphic for IRC Thread", 
			"Secondary  Mandate: Detect Pixeltail Developer Twitch Streams",
			"Tertiary   Mandate: Log IRC (http://www.gamechaser.net/pixeltail)",
			"Quaternary Mandate: Detect Trello Updates for Tower Unite Roadmap.",
			"Quinary    Mandate: Replace Internets Bot by Replicating Functionality.",
			"-----------------------",
			"IRC Chat Logs:    http://www.gamechaser.net/pixeltail",
			"Samaritan Trello: https://trello.com/b/xix2BF1i/samaritan"
			};
			
			String[] indiegogo = {
			"Tower Unite Indiegogo Final Total:", 
			"$73,633 USD raised by 2,051 people.", 
			"https://www.indiegogo.com/projects/tower-unite"};
			
			String[] exitMessage = {
			"Pattern Recognition: Offline",
			"Analysis:   Offline",
			"Heuristics: Offline",
			"Samaritan:  Offline"
			};
			
			String[] helpMessage = {
			"Operational Directives:",
			"-----------------------",
			"!igg    (Provides Indiegogo Information for Tower Unite)",
			"!twitch (Checks to see if any Pixeltail Developers are Streaming)",
			"!info-samaritan (Displays Information about Samaritan)",
			"!g (Google Search)        | Syntax: !g  *SEARCH TERM HERE*",
			"!gi (Google Image Search) | Syntax: !gi *SEARCH TERM HERE*",
			"!yt (Youtube Search)      | Syntax: !yt *SEARCH TERM HERE*",
			"!w (Weather Search)       | Syntax: !w  *CITY,COUNTRY*",
			"!define (Dictionary)      | Syntax: !define *WORD HERE*",
			"-----------------------",
			"Alternatively, you may privately message Samaritan using the above commands",
			"and it will respond to you that way."
			};

			// FTP Server Details
		    ArrayList<String> FTPInfo = Credentials.GetFTPData();
			String FTPServer   = FTPInfo.get(0);
			String FTPUsername = FTPInfo.get(1);
			String FTPPassword = FTPInfo.get(2);
			
			// Forum Image Location
			ArrayList<String> ImageInfo = Credentials.GetImageData();
			String BackgroundImage = ImageInfo.get(0);
			String FinalImage      = ImageInfo.get(1);
			
			// IRC Server Details
			ArrayList<String> IRCInfo = Credentials.GetIRCData();
			String IRCServer   = IRCInfo.get(0);
			String IRCNick     = IRCInfo.get(1);
			String IRCPassword = IRCInfo.get(2);
			String IRCRealname = IRCInfo.get(3);
			String IRCChannel  = IRCInfo.get(4);
			
			// Twitch Names
			Map <Integer, String> TwitchNames = new HashMap<Integer, String>();
			TwitchNames.put(88752545, "Pixeltail Games");
			TwitchNames.put(2185137,  "MacDGuy");
			TwitchNames.put(6854939,  "Foohy");
			TwitchNames.put(32039860, "Matt");
			TwitchNames.put(22899421, "Lifeless");
			TwitchNames.put(46510960, "Zak");
			
			// Twitch URLs
			Map <Integer, String> TwitchURLs = new HashMap<Integer, String>();
			TwitchURLs.put(88752545, "http://www.twitch.tv/pixeltailgames");
			TwitchURLs.put(2185137,  "http://www.twitch.tv/macdguy");
			TwitchURLs.put(6854939,  "http://www.twitch.tv/foohy");
			TwitchURLs.put(32039860, "http://www.twitch.tv/gamedev11");
			TwitchURLs.put(22899421, "http://www.twitch.tv/lifeless2011");
			TwitchURLs.put(46510960, "http://www.twitch.tv/zakblystone");

			// Connect to IRC Server
			Socket socket = new Socket(IRCServer, 6667);
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			System.out.println(new Timestamp(System.currentTimeMillis()) + ": Established Server Connection.");
			
			// Kill Any Ghosts
			writer.write("NICKSERV GHOST " + IRCNick + " " + IRCPassword + "\r\n");
			writer.flush();
			
			// Login to Server
			writer.write("NICK " + IRCNick + "\r\n");
			writer.write("USER " + IRCNick + " 8 * : " + IRCRealname + "\r\n");
			writer.flush();
			
			// Read lines from server for connection.
			String line = null;
			boolean secondName = false;
			long systemTimeMilli = 0;
			while((line = reader.readLine()) != null) {
				if(line.indexOf("004") >= 0) {
					// Login Success
					break;
				} else if(line.indexOf("433") >= 0) {
					writer.write("NICK Samaritan2 \r\n");
					writer.write("NICKSERV IDENTIFY " + IRCPassword  + "\r\n");
					writer.flush();
					secondName = true;
					systemTimeMilli = System.currentTimeMillis();
					System.out.println("Nickname already in use. Switching to backup name.");
					break;
				}
			}
			
			System.out.println(new Timestamp(System.currentTimeMillis()) + ": Login Successful.");
			
			// Join Channel
			writer.write("JOIN " + IRCChannel + "\r\n");
			writer.flush();
			System.out.println(new Timestamp(System.currentTimeMillis()) + ": Joined Channel.");
			
			// Login
			writer.write("NICKSERV IDENTIFY " + IRCPassword  + "\r\n");
			writer.flush();
			
			// Begin TwitchThread
			Thread tTwitchThread = new Thread(new TwitchThread(writer, IRCChannel, TwitchNames, TwitchURLs));
			if(!tTwitchThread.isAlive()) {
				tTwitchThread.start();
				System.out.println(new Timestamp(System.currentTimeMillis()) + ": Twitch Thread Started.");
			} else {
				System.out.println(new Timestamp(System.currentTimeMillis()) + ": Twitch Thread Already Running.");
			}
			
			// Logging
			DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
			DateFormat sigFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm a zzz");
			DateFormat hourFormat = new SimpleDateFormat("HH");
			
			Date initDate = new Date(System.currentTimeMillis());
			int systemHour = Integer.parseInt(hourFormat.format(initDate));
			String dateInit = dateFormat.format(initDate);
			String logName = "towerunitelog/" + dateInit + ".txt";
			
			// Begin TrelloThread
			Thread tTrelloThread = new Thread(new TrelloThread(writer, IRCChannel, Trello.getEndTime()));
			if(!tTrelloThread.isAlive()) {
				tTrelloThread.start();
				System.out.println(new Timestamp(System.currentTimeMillis()) + ": Trello Thread Started.");
			} else {
				System.out.println(new Timestamp(System.currentTimeMillis()) + ": Trello Thread Already Running.");
			}

			// Start Functionality
			boolean isSpeech = false;
			boolean checking = false;
			String ircNick = null;
			String ircCommand = null;
			String ircMessage = null;
			String ircParams = null;
			
			LogHelper.add(logName, "LOG NOTICE", "SYSTEM ONLINE. LOGGING RESUMED!", IRCChannel);
			Pattern pattern = Pattern.compile("(?<rawMessage>\\:(?<source>((?<nick>[^!]+)![~]{0,1}(?<user>[^@]+)@)?(?<host>[^\\s]+)) (?<command>[^\\s]+)( )?(?<parameters>[^:]+){0,1}(:)?(?<text>.*[^\r^\n^])?)");
			Pattern youtubePattern = Pattern.compile("^(?:https?://)?(?:www\\.)?(?:youtu\\.be\\/|youtube\\.com\\/(?:embed\\/|v\\/|watch\\?v=|watch\\?.+&v=))((\\w|-){11})(?:\\S+)?$");
			
			while((line = reader.readLine()) != null) {
				// Reset Channel
				IRCChannel = IRCInfo.get(4);
				
				// REGEX for Parsing IRC
				Matcher matcher = pattern.matcher(line);
				
				// String Setup
				String ex[] = line.split("\\s+");
				if(matcher.find()) {
					ircNick = matcher.group("nick");
					ircCommand = matcher.group("command");
					ircMessage = matcher.group("text");
					ircParams = matcher.group("parameters");
					
					if(ircCommand.equals("PRIVMSG")) {
						if(ircMessage == null) {
							ircMessage = "^";
						}
						if(!ircParams.replaceAll("\\s","").equals(IRCChannel)) {
							IRCChannel = ircNick;
						}
						isSpeech = true;
					}
				}
				
				// Alternative Name Check
				if(secondName) {
					long currentTimeMilli = System.currentTimeMillis();
					if((currentTimeMilli - systemTimeMilli) >= 30000) {
						if(checking) {
							if(line.indexOf("433") >= 0) {
								System.out.println("Username Taken. Aborted.");
								checking = false;
								systemTimeMilli = currentTimeMilli;
								continue;
							}
							writer.write("NICKSERV IDENTIFY " + IRCPassword  + "\r\n");
							writer.flush();
							secondName = false;
						}
						writer.write("NICK " + IRCNick + "\r\n");
						writer.flush();
						checking = true;
					}
				}
				
				// Logging Setup
				Date tempDate = new Date(System.currentTimeMillis());
				int currentHour = Integer.parseInt(hourFormat.format(tempDate));

				// Check if Update Needed
				//&& Credentials.Debug != 1
				if(systemHour != currentHour && Credentials.Debug != 1) {
					
					// Upload Log
					FTPHelper.upload(logName, FTPServer, FTPUsername, FTPPassword, "/" + logName, FTP.ASCII_FILE_TYPE);
					
					// Set systemHour to currentHour
					systemHour = currentHour;
					
					// Make new Log File
					if(systemHour == 0 || systemHour == 24) {
						String newDate = dateFormat.format(tempDate);
						logName = "towerunitelog/" + newDate + ".txt";
						File log = new File(logName);
						if(!log.exists()) {
							PrintWriter pWrite = new PrintWriter(logName, "UTF-8");
							pWrite.close();
						}
					}
				}
				
				// Add to Log
				if(isSpeech && !LogHelper.isBlocked(ircNick)) {
					LogHelper.add(logName, ircNick, ircMessage, IRCChannel);	
				}
				
				// Check YouTube
				if(isSpeech) {
					if(ircMessage.length() > 0) {
						Matcher youtubeMatcher = youtubePattern.matcher(ircMessage);
						if(youtubeMatcher.find()) {
							String videoID = youtubeMatcher.group(1);
							if(videoID.length() >= 11) {
								String[] youtubeResponse = YouTubeSearch.retrieve(videoID);
								IRCHelper.write(writer, IRCChannel, youtubeResponse);
								LogHelper.add(logName, "Samaritan", youtubeResponse, IRCChannel);
							}
						}
					}
				}
				isSpeech = false;
				
				// PRIVMSG Detection
				if(ex.length >= 4) {
					if(ircMessage != null) {
						// Google Search
						if(ircMessage.startsWith("!g ")) {
							if(ircMessage.substring(3).length() > 0) {
								System.out.println(new Timestamp(System.currentTimeMillis()) + ": !g Command Run");
								
								String[] googleResult = GoogleSearch.search(ircMessage.substring(3));
								IRCHelper.write(writer, IRCChannel, googleResult);
								LogHelper.add(logName, "Samaritan", googleResult, IRCChannel);
							}
							continue;
							// Google Image Search
						} else if(ircMessage.startsWith("!gi ")) {
							if(ircMessage.substring(4).length() > 0) {
								System.out.println(new Timestamp(System.currentTimeMillis()) + ": !gi Command Run");
								
								String[] googleImageResult = GoogleImageSearch.search(ircMessage.substring(4));
								IRCHelper.write(writer, IRCChannel, googleImageResult);
								LogHelper.add(logName, "Samaritan", googleImageResult, IRCChannel);
							}
							continue;
							// YouTube Search
						} else if(ircMessage.startsWith("!yt ")) {
							if(ircMessage.substring(4).length() > 0) {
								System.out.println(new Timestamp(System.currentTimeMillis()) + ": !yt Command Run");

								String[] youtubeResult = YouTubeSearch.search(ircMessage.substring(4));
								IRCHelper.write(writer, IRCChannel, youtubeResult);
								LogHelper.add(logName, "Samaritan", youtubeResult, IRCChannel);
							}
							continue;
							// Weather Search
						} else if (ircMessage.startsWith("!w ")) {
							if(ircMessage.substring(3).length() > 0) {
								System.out.println(new Timestamp(System.currentTimeMillis()) + ": !w Command Run");

								String[] weatherResult = Weather.get(ircMessage.substring(3));
								IRCHelper.write(writer, IRCChannel, weatherResult);
								LogHelper.add(logName, "Samaritan", weatherResult, IRCChannel);
							}
							continue;
							// Dictionary Lookup
						} else if (ircMessage.startsWith("!define ")) {
							if(ircMessage.substring(8).length() > 0) {
								System.out.println(new Timestamp(System.currentTimeMillis()) + ": !define Command Run");

								String[] dictionaryResult = Dictionary.lookup(ircMessage.substring(8));								
								IRCHelper.write(writer, IRCChannel, dictionaryResult);
								LogHelper.add(logName, "Samaritan", dictionaryResult, IRCChannel);
							}
							continue;
						}
					}
					
					if(ircCommand.equals("PRIVMSG")) {
						if(ircMessage.startsWith("!")) {
							// Shutdown Command
							if(ircMessage.equals("!suspend") && ircNick.equals("Caboose700")) {
								LogHelper.add(logName, "LOG NOTICE", "SYSTEM SHUTDOWN. LOGGING STOPPED!", IRCChannel);
								IRCHelper.write(writer, IRCChannel, exitMessage);
								Trello.recordEndTime();
								System.exit(0);
							}
														
							switch(ircMessage) {
							case "!info-samaritan":
								System.out.println(new Timestamp(System.currentTimeMillis()) + ": !info-samaritan Command Run");
								IRCHelper.notice(writer, ircNick, info);
							break;
							case "!igg":
								System.out.println(new Timestamp(System.currentTimeMillis()) + ": !igg Command Run");
								IRCHelper.write(writer, IRCChannel, indiegogo);
								LogHelper.add(logName, "Samaritan", indiegogo, IRCChannel);
							break;
							case "!twitch":
								System.out.println(new Timestamp(System.currentTimeMillis()) + ": !twitch Command Run");
								ArrayList<Integer> twitchResult = Twitch.check(writer, IRCChannel, TwitchNames, TwitchURLs);
								if(twitchResult.size() == 0) {
									String[] twitchInfo = {"[Twitch] No Pixeltail developers are currently streaming."};
									IRCHelper.write(writer, IRCChannel, twitchInfo);
								}
								LogHelper.add(logName, "LOG NOTICE", "Samaritan Response Not Logged", IRCChannel);
							break;
							case "!help":
								System.out.println(new Timestamp(System.currentTimeMillis()) + ": !help Command Run");
								IRCHelper.notice(writer, ircNick, helpMessage);
							break;
							default:
								// Do nothing
							break;
							}
							continue;
						}
					}
				}

				// Respond to PING Commands
				if(line.startsWith("PING")) {
					writer.write("PONG " + line.substring(5) + "\r\n");
					writer.flush();
					System.out.println(new Timestamp(System.currentTimeMillis()) + ": Pong Sent.");
				// Detect NAMES Command
				} else if(line.indexOf("353") >= 0 && IRCChannel == "#TowerUnite") {
					// Get Users
					int usercount = ex.length - 5;
					
					// Create and Save Image
					BufferedImage img = null;
					img = ImageIO.read(new File(BackgroundImage));
				    Graphics g = img.getGraphics();
				    g.setFont(new Font("Arial", Font.BOLD, 11));
				    g.drawString("The Unofficial Tower Unite IRC Chat!", 100, 25);
				    g.drawString("Server: irc.rizon.net Channel: #TowerUnite", 100, 39);
				    g.drawString(usercount + " online user(s)", 100, 52);
				    g.drawString("Last Updated:", 423, 39);
				    g.drawString(sigFormat.format(tempDate), 423, 52);
				    g.dispose();
				    ImageIO.write(img, "png", new File(FinalImage));
				    
				    // Upload Image to FTP Server
				    FTPHelper.upload(FinalImage, FTPServer, FTPUsername, FTPPassword, "toweruniteIRC.png", FTP.BINARY_FILE_TYPE);
					
				// Reconnect on Disconnect
				} else if(line.startsWith("ERROR :Closing Link:")) {
					System.out.println(new Timestamp(System.currentTimeMillis()) + ": ERROR: Server Disconnected. Initiating System Reboot.");
					socket.close();
					System.out.println(new Timestamp(System.currentTimeMillis()) + ": Samaritan Offline.\r\n");
					break;
				}
				
				// Detect if User Join, Park, Quit, Kick, Nick, or Ban
				switch(ircCommand) {
					case "JOIN":
						LogHelper.add(logName, "LOG NOTICE", ircNick + " has joined the channel.", IRCChannel);
					break;
					case "PART":
						LogHelper.add(logName, "LOG NOTICE", ircNick + " has left the channel.", IRCChannel);
					break;
					case "QUIT":
						LogHelper.add(logName, "LOG NOTICE", ircNick + " has left the channel.", IRCChannel);
					break;
					case "KICK":
						String[] kickParams = ircParams.split("\\s+");
						String kickedUser = kickParams[1];
						String kickReason = ircMessage.equals(kickedUser) ? "No Reason Specified." : ircMessage;
						LogHelper.add(logName, "LOG NOTICE", ircNick + " kicked " + kickedUser + " from the channel. Reason: \"" + kickReason + "\"", IRCChannel);
					break;
					case "NICK":
						LogHelper.add(logName, "LOG NOTICE", ircNick + " is now known as " + ircMessage + ".", IRCChannel);
					break;
					case "MODE":
						String[] modeParams = ircParams.split("\\s+");
						if(modeParams.length >= 3) {
							String modeStatus = modeParams[1];
							String affectedUser = modeParams[2];
							if(modeStatus.equals("+b") || modeStatus.equals("-b")) {
								String banStatus = modeStatus.equals("+b") ? "banned" : "unbanned";
								LogHelper.add(logName, "LOG NOTICE", ircNick + " " + banStatus + " " + affectedUser + " from the channel.", IRCChannel);
							} else if(modeStatus.equals("+h") || modeStatus.equals("-h")) {
								String halfopStatus = modeStatus.equals("+h") ? "gives channel half-operator status to" : "removes channel half-operator status from";
								LogHelper.add(logName, "LOG NOTICE", ircNick + " " + halfopStatus + " " + affectedUser + ".", IRCChannel);
							} else if(modeStatus.equals("+v") || modeStatus.equals("-v")) {
								String voiceStatus = modeStatus.equals("+v") ? "gives voice to" : "removes voice from";
								LogHelper.add(logName, "LOG NOTICE", ircNick + " " + voiceStatus + " " + affectedUser + ".", IRCChannel);
							} else if(modeStatus.equals("+q") || modeStatus.equals("-q")) {
								String ownerStatus = modeStatus.equals("+q") ? "gives channel owner status to" : "removes channel owner status from";
								LogHelper.add(logName, "LOG NOTICE", ircNick + " " + ownerStatus + " " + affectedUser + ".", IRCChannel);
							} else if(modeStatus.equals("+o") || modeStatus.equals("-o")) {
								String opStatus = modeStatus.equals("+o") ? "gives channel operator status to" : "removes channel operator status from";
								LogHelper.add(logName, "LOG NOTICE", ircNick + " " + opStatus + " " + affectedUser + ".", IRCChannel);
							}
						}
					break;
				}
				// Send Names Command
				if(ircCommand.equals("JOIN") || ircCommand.equals("PART") || ircCommand.equals("QUIT") || ircCommand.equals("KICK") || ircCommand.equals("NICK") || ircCommand.equals("MODE")) {
					writer.write("NAMES " + IRCChannel + "\r\n");
					writer.flush();
				}
			}
			continue;	
		} while (running);
	}
}