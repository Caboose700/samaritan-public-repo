# Samaritan v.6.5.2#

Samaritan is the IRC Bot originally responsible for the #pixeltail and #towerunite IRC Channels. Development ceased on September 18th, 2015, and no other updates are planned. I can offer no help to someone attempting to build the .jar from these source files, but you SHOULD be able to figure that out.

Source Code is released for anyone to use as a learning experience and do what they will with it. All the dependencies are also included. There are probably bugs lurking in the source code, feel free to fix them for fun ;)

### Includes Folder ###
The includes folder includes the background image required for generating the updating graphic.

### Required Setup ###
Before executing the Samaritan.jar file that you compile, make sure that a folder named "towerunitelog", and a .txt file named "date.txt" that contains only "2015-09-19T03:13:06.783Z". These two objects MUST be in the same directory as Samaritan.jar or else it will crash. The "date.txt" file is for the Trello Thread to keep track of when it last checked Trello.

### Credentials.java ###
This file contains all of the bot's login credentials and API keys. Make sure to fill in every single line with data. It is not known how Samaritan will operate if nothing is passed in, but it would most likely crash.

